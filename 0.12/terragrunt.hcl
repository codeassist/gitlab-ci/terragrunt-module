locals {
  # by default: nntek AWS account ID
  aws_account_id = get_env("AWS_ACCOUNT_ID", "242031741314")

  # assume role defined within the specified account to apply tf configuration
  aws_assume_iam_role = get_env("AWS_ASSUME_IAM_ROLE", "arn:aws:iam::${local.aws_account_id}:role/operator-service-role")
}

iam_role = local.aws_assume_iam_role

remote_state {
  backend = "local"

  config = {}
}

# Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
# working directory, into a temporary folder, and execute your Terraform commands in that folder.
terraform {

  extra_arguments "terraform_tfvars" {
    commands = get_terraform_commands_that_need_vars()

    optional_var_files = [
      "${get_terragrunt_dir()}/terraform.tfvars"
    ]
  }

  # Terragrunt will copy the Terraform configurations specified by the source parameter, along with any files in the
  # working directory, into a temporary folder, and execute your Terraform commands in that folder.

  # Note: the double slash (//) is intentional and required. It's part of Terraform's Git syntax for module sources.
  # See: https://www.terraform.io/docs/modules/sources.html
  # Terraform may display a "Terraform initialized in an empty directory" warning, but you can safely ignore it.)
  source = "../.."
}
