variable "aws_region" {
  description = "AWS provider requires region to be set."
  type        = string
  default     = "us-east-1"
}

provider "aws" {
  region = var.aws_region
}
