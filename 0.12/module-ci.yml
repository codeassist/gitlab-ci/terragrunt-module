---
# As described in:
#   * https://docs.gitlab.com/ce/ci/yaml/#include
include:
  - remote: https://gitlab.com/codeassist/gitlab-ci/semantic-versioning/raw/master/.semver-ci.yml


stages:
  - test
  - versioning


variables:
  # Related to the issue when jobs randomly fails with error "fatal: No such remote 'origin'". See
  #   * https://gitlab.com/gitlab-org/gitlab-runner/issues/3590
  #   * https://gitlab.com/gitlab-org/gitlab-runner/issues/3599
  # Though this strategy is noted to be slower than fetch. And this feature is also noted to be an experimental, so it
  # might be removed in the future too. So this is workaround only for now (01/04/2019).
  GIT_STRATEGY: clone
  # Used to control if / how Git submodules are included when fetching the code before a build. Can be set globally or
  # per-job in the variables section. There are three possible values: 'none', 'normal', and 'recursive'.
  # See: https://docs.gitlab.com/ce/ci/yaml/README.html#git-submodule-strategy
  GIT_SUBMODULE_STRATEGY: none
  # When using dind, it's wise to use the overlayfs driver for
  # improved performance.
  DOCKER_DRIVER: overlay2
  # Test against Terraform version
  TERRAFORM_VERSION: "0.12"


test:module:plan:
  stage: test
  image: registry.gitlab.com/codeassist/docker/docker-images/terraform/${TERRAFORM_VERSION}-docker-awscli
  before_script:
    # print versions used in test env
    - terraform -version
    - terragrunt -version

    # get archive with test config files from the repo
    - wget -q https://gitlab.com/codeassist/gitlab-ci/terragrunt-module/-/archive/master/package.zip
    # unpack the archive. After that all files will be placed in './terragrunt-module-master-*/"${TERRAFORM_VERSION}"/'
    - unzip -o package.zip

    # change working directory to the specific for desired version
    - cd ./terragrunt-module-master-*/"${TERRAFORM_VERSION}"/
  script:
    # if directory with test was found - perform tests against different sets of variables
    - |
      if [ -d "${CI_PROJECT_DIR}/tests" ];
      then
        for testdir in `find "${CI_PROJECT_DIR}/tests/" -maxdepth 1 -mindepth 1 -type d`; do
          # copy all test resources from test directory into current working directory
          cp -f ${testdir}/* ./;
          echo -e "\e[34m`date +'%F %T'` [*INFO] - running test from [${testdir}] with set of vars:\e[0m";
          # if there is 'terraform.tfvars' file found - print it content in the console
          echo -e "\e[36m"
          tail -n +1 ./terraform.tfvars;
          echo -e "\e[0m"
          # try to compute a TF plan (do not lock anything as it's only temporary test environment; all variables must
          # be already set as it's noninteractve environment)
          terragrunt plan -input=false -lock=false;
        done;
      else
        echo -e "\e[34m`date +'%F %T'` [*INFO] - no tests found, nothing to do...\e[0m";
      fi;
  only:
    - branches
    - merge_requests
  except:
    - develop
    - master
  tags:
    - docker


version:tag:increment:
  # config section from additionally included file
  extends: .semantic_versioning
  # base config
  stage: versioning
  image: python:3.7-stretch
  before_script:
    # get semantic versioning repo archive
    - wget -q https://gitlab.com/codeassist/gitlab-ci/semantic-versioning/-/archive/master/semantic-versioning-master.zip
    # unpack the archive
    - unzip -j -o semantic-versioning-master.zip
  only:
    - merge_requests
  tags:
    - docker
